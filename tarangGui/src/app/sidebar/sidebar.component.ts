import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public settings: Array<any>;
  constructor() {
  	this.settings = [
  		{name: "PROGRAM", path: "/program"}, 
  		{name: "FIELD", path: "/field"}, 
  		{name: "PHYSICS", path: "/physics"},
  		{name: "TIME", path: "/path"}, 
  		{name: "FINISH", path: "/finish"}
  	]
  }

  ngOnInit() {
  }

}
