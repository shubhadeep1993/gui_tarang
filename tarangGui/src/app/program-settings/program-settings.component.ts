import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { SettingsService } from '../settings.service';

class Switch {
	selectedValue: string;
	possibleValues: Array<string>; //Max length 2
	description: string;
	public Switch(){};
}

class Select {
	selectedValue: string;
	possibleValues: Array<string> = [];
	description: string;
	public Select(){};
}

@Component({
  selector: 'app-program-settings',
  templateUrl: './program-settings.component.html',
  styleUrls: ['./program-settings.component.css']
})
export class ProgramSettingsComponent implements OnInit {

  public configuration = {
	kind: new Select(),
	basis_type: new Select(),
	decomposition: new Select(),
	iter_or_diag: new Switch(),
	alias_option: new Switch(),
	integration_scheme: new Select(),
	LES_switch: new Switch(),
	apply_strong_realitycond_alltime_switch: new Switch(),
	apply_weak_realitycond_alltime_switch: new Switch(),
	low_dimension_switch: new Switch(),
	two_and_half_dimension: new Switch(),
	two_dimension: new Switch(),
	dt_option: new Switch(),
	helicity_switch: new Switch(),
	sincostr_switch: new Switch()
  };

  private configurationNames: Array<string>;
  private descriptionConf: string;

  constructor(private settings: SettingsService, private CDRef: ChangeDetectorRef) {
	this.configurationNames = Object.keys(this.configuration);
  }

  onSelectKind(kind) {
    console.log(kind);
  	if (kind == "MHD") {
  		this.configuration.basis_type.possibleValues = [ "SFF" ];
  	} else {
  		this.configuration.basis_type.possibleValues = ["FFF", "SSS"];
  	}
  }

  ngOnInit() {
  	setTimeout(() => {
		this.populateSettings()
	}, 1000);
  }

  populateSettings() {
	for (var i = 0; i < this.configurationNames.length; i++) {
		var key = this.configurationNames[i];
		this.configuration[key].possibleValues = this.settings[key].values;
		this.configuration[key].selectedValue = this.settings[key].currentvalue;
		this.configuration[key].description = this.settings[key].description;
	}
	this.descriptionConf = this.configurationNames[0];
  }

  showDescription(key, evt) {
	this.descriptionConf = key;
	if (this.validate(key, evt.target.value)) {
		this.configuration[key].selectedValue = evt.target.value;
		this.settings[key].currentvalue = evt.target.value;
	} else {
		this.configuration[key].selectedValue = "";
		this.settings[key].currentvalue = "";
	}
	this.CDRef.detectChanges()
	console.log("Current values: ", this.settings[key].currentvalue)
  }

  isDropdown(conf) {
	return this.configuration[conf] instanceof Select;
  }


  validate(key, value) {
	validatekind(key, value);
	// validateIntegration(key, value);
	// validateTimescheme(key, value);
  }

  validatekind(key, value) {
	if (key == "kind" && value == "RBC") {
  	  alert("Cannot set RBC")
  	  return false;
  	}
  	return true;
  }
}
