import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramSettingsComponent } from './program-settings.component';

describe('ProgramSettingsComponent', () => {
  let component: ProgramSettingsComponent;
  let fixture: ComponentFixture<ProgramSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
