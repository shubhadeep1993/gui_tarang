import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export class Conf {
	public currentvalue: string;
	public values: Array<string>;
	public description: string;

	constructor(conf) {
		this.currentvalue = conf.values[0];
		this.values = conf.values;
		this.description = conf.description;
	}
}

@Injectable()
export class SettingsService {

  private kind: Conf;
  private basis_type: Conf;
  public decomposition: Conf;
  public iter_or_diag: Conf;
  public alias_option: Conf;
  public integration_scheme: Conf;
  public LES_switch: Conf;
  public apply_strong_realitycond_alltime_switch: Conf;
  public apply_weak_realitycond_alltime_switch: Conf;
  public low_dimension_switch: Conf;
  public two_and_half_dimension: Conf;
  public two_dimension: Conf;
  public dt_option: Conf;
  public helicity_switch: Conf;
  public sincostr_switch: Conf;

  constructor(private http: HttpClient) {
  	this.http.get<any>("./assets/programConfig.json").subscribe((conf) => {
		var keys = Object.keys(conf.program);
		for (var i = 0; i < keys.length; i++) {
			var key = keys[i];
			this[key] = new Conf(conf.program[key]);
		}
  	})
  }

  GetKinds() {
  	return this.kind;
  }

  GetBasisTypes() {
  	return this.basis_type;
  }

  GetYAML() {
	  return `
	  program:
	  	kind: "${this.kind.currentvalue}",
		basis_type: "${this.basis_type.currentvalue}",
		decomposition: "${this.decomposition.currentvalue}",
		iter_or_diag: "${this.iter_or_diag.currentvalue}",
		alias_option: "${this.alias_option.currentvalue}",
		integration_scheme: "${this.integration_scheme.currentvalue}",
		LES_switch: "${this.LES_switch.currentvalue}",
		apply_strong_realitycond_alltime_switch: ${this.apply_strong_realitycond_alltime_switch.currentvalue},
		apply_weak_realitycond_alltime_switch: ${this.apply_weak_realitycond_alltime_switch.currentvalue},
		low_dimension_switch: ${this.low_dimension_switch.currentvalue},
		two_and_half_dimension: ${this.two_and_half_dimension.currentvalue},
		two_dimension: ${this.two_dimension.currentvalue},
		dt_option: ${this.dt_option.currentvalue},
		helicity_switch: ${this.helicity_switch.currentvalue},
		sincostr_switch: ${this.sincostr_switch.currentvalue}
	  `
  }
}
