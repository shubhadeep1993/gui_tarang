import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { ProgramSettingsComponent } from './program-settings/program-settings.component';
import { SettingsService } from './settings.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FieldSettingsComponent } from './field-settings/field-settings.component';
import { ToggleSwitchComponent } from './toggle-switch/toggle-switch.component';
import { FinishComponent } from './finish/finish.component';

const appRoutes: Routes = [
  { path: 'program', component: ProgramSettingsComponent },
  { path: 'field',   component: FieldSettingsComponent },
  { path: 'finish',  component: FinishComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ProgramSettingsComponent,
    SidebarComponent,
    FieldSettingsComponent,
    ToggleSwitchComponent,
    FinishComponent
  ],
  imports: [
   RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
	FormsModule,
    HttpClientModule
  ],
  providers: [ SettingsService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
