import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-finish',
  templateUrl: './finish.component.html',
  styleUrls: ['./finish.component.css']
})
export class FinishComponent implements OnInit {
  private downloadstring: string;
  constructor(private settings: SettingsService) { }

  ngOnInit() {
	this.downloadstring = "data:application/octet-stream;charset=utf-16le;base64,//5mAG8AbwAgAGIAYQByAAoA";
  }

  download() {
	var blob = new Blob([this.settings.GetYAML()], { type: 'text/yaml' });
    var url= window.URL.createObjectURL(blob);
    window.location.href = url;
  }

}
