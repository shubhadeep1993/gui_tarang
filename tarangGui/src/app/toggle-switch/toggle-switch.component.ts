import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-toggle-switch',
  templateUrl: './toggle-switch.component.html',
  styleUrls: ['./toggle-switch.component.css']
})
export class ToggleSwitchComponent implements OnInit {

  @Input()
  Option1: string;

  @Input()
  Option2: string;

  @Input()
  currentvalue: string;

  @Output()
  toggleEvent = new EventEmitter<any>();

  public Opt1Active: boolean = true;

  constructor() {
  }

  ngOnInit() {
	if (this.currentvalue == this.Option1)
		this.Opt1Active = true;
  }

  toggle() {
	this.Opt1Active = !this.Opt1Active;
	if (this.Opt1Active) {
		this.toggleEvent.emit({ target: { value: this.Option1 }});
	} else {
		this.toggleEvent.emit({ target: { value: this.Option2 }});
	}
  }

}
